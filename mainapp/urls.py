from django.urls import path

from .views import HomepageView, ProductDetailView, CategoryDetailView, CartView, AddToCartView

urlpatterns = [
    path('', HomepageView.as_view(), name='homepage'),
    path('products/<str:content_type_model>/<str:slug>/', ProductDetailView.as_view(), name='product_details'),
    path('category/<str:slug>/', CategoryDetailView.as_view(), name='category_detail'),
    path('cart/', CartView.as_view(), name='cart'),
    path('add-to-cart/<str:content_type_model>/<str:slug>/', AddToCartView.as_view(), name='add_to_cart')
]
